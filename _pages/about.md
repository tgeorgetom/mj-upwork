---
layout: page
title: JobSite Template for Jekyll
permalink: /about
comments: true
---

<div class="row justify-content-between">
<div class="col-md-8 pr-5">

<p>This website is built with Jekyll and JobSite template for Jekyll. It's for demonstration purposes, no real content can be found. JobSite template for Jekyll is compatible with Github pages, in fact even this demo is created with Github Pages and hosted with Github.</p>

<p class="mb-5"><img class="shadow-lg" src="{{site.baseurl}}/assets/images/mediumish-jekyll-template.png" alt="jekyll template mediumish" /></p>
<h4>Documentation</h4>

<p>Please, read the docs <a href="#">here</a>.</p>

<h4>Questions or bug reports?</h4>

<p>Head over to our <a href="#">Github repository</a>!</p>

</div>

<div class="col-md-4">

<div class="sticky-top sticky-top-80">
<h5>Buy me a coffee</h5>
<p>Thank you for your support! Your donation helps me to maintain and improve</p>

</div>
</div>
</div>
